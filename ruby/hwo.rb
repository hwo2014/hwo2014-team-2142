class Game
  # Used to store current game, including 'my car'.
  # When race data (with track, cars and session data) comes in, this
  # will be added as well. The current game tick can be updated.

  attr_reader :game_id, :my_car
  attr_accessor :race, :current_tick

  def initialize(game_id, my_car)
    @game_id = game_id
    @my_car = my_car
  end
end

class Race
  # Used to store data on a race: track (including pieces, lanes),
  # session and list of cars. A race belongs to a game.

  attr_reader :track, :session, :cars

  def initialize(data)
    @track = Track.new(data["track"])

    @session = Session.new(data["raceSession"])

    @cars = []
    data["cars"].each do |car_data|
      @cars << Car.new(car_data)
    end
  end
end


class Session
  # Stores basic session data: laps, maximum lap time and whether
  # the race is a quick race. A session belongs to a race.

  attr_reader :laps, :max_lap_time_ms, :quick_race

  def initialize(data)
    @laps = data["laps"]
    @max_lap_time_ms = data["maxLapTimeMs"]
    @quick_race = data["quickRace"]
  end
end


class Car
  # Used to store basic data on cars, including a list of positions.
  # Car position is a hash combining position and tick.
  # A car (in a list) belongs to a race.

  attr_reader :name, :color, :length, :width, :guide_flag_position
  attr_reader :positions

  def initialize(data)
    @name = data["name"]
    @color = data["color"]
    @positions = []
  end

  def set_dimensions(data)
    @width = data["width"]
    @length = data["length"]
    @guide_flag_position = data["guideFlagPosition"]
  end

  def add_position(position, tick)
    # Test that current game tick is later than last one received.
    last_tick = @positions.length > 0 ? @positions.last[:tick] : -1

    if tick > last_tick
      @positions << {:position => position, :tick => tick}
    end
  end

  def current_position
    @positions.last[:position]
  end

  def current_velocity(track)
    # Cannot compute speed if there are not two positions stored
    return 0 if @positions.length < 2
    current_position = @positions.last
    previous_position = @positions[-2]

    # Compute time difference
    time_difference = current_position[:tick] - previous_position[:tick]

    # Compute distance
    distance = track.distance_between_two_positions(current_position, previous_position)

    # Compute and return speed
    if time_diff != 0
      distance / time_difference
    else
      0
    end
  end

  def current_acceleration
  end
end


class Track
  # Stores information on a track, including the pieces and lanes
  # that are part of the track. A track belongs to a race.

  attr_reader :id, :name, :pieces, :lanes

  def initialize(data)
    @id = data["id"]

    @name = data["name"]

    @pieces = []
    data["pieces"].each do |piece_data|
      @pieces << Piece.new(piece_data)
    end

    @lanes = []
    data["lanes"].each do |lane_data|
      @lanes << Lane.new(lane_data)
    end if data["lanes"]
  end

  def upcoming_pieces(starting_piece_index, length)
    ((starting_piece_index..@pieces.length-1).to_a + (0..starting_piece_index-1).to_a)[0,length]
  end

  def switch_before_turn?(starting_piece_index)
    piece_indexes_to_consider = upcoming_pieces(starting_piece_index, @pieces.length)
    
    upcoming_switch = piece_indexes_to_consider.find_index { |i| @pieces[i].switch? }
    upcoming_turn = piece_indexes_to_consider.find_index { |i| @pieces[i].bend? }

    # It could be there there is no switch at all...
    return false if (upcoming_switch == nil || upcoming_turn == nil)

    # Return whether order of upcoming switch is lower than upcoming turn.
    # Upcoming switch should not be first piece in order, because there is no way to use it.
    (upcoming_switch <= upcoming_turn) && (upcoming_switch > 0)
  end

  def length_of_straight_pieces_ahead(starting_piece_index)
    piece_indexes_to_consider = upcoming_pieces(starting_piece_index, @pieces.length)

    upcoming_turn = piece_indexes_to_consider.find_index { |i| @pieces[i].bend? }
    
    # Possibly, no turn at all...
    return total_pieces if upcoming_turn == nil

    # Return number of first piece that is a turn
    upcoming_turn
  end

  def amount_of_turn_ahead(starting_piece_index, pieces_to_look_ahead)
  end

  def distance_between_two_positions(position1, position2)
  end
end


class Piece
  # Represents one piece (straight or bend) of a track,
  # and might contain a switch of lanes. A piece belongs to a track.

  attr_reader :radius, :angle
  
  def initialize(data)
    if data["length"]
      @type = "straight"
      @length = data["length"]
    elsif data["angle"]
      @type = "bend"
      @radius = data["radius"]
      @angle = data["angle"]
    end
    @switch = data["switch"] ? true : false
  end
  
  def switch?
    @switch
  end

  def straight?
    @type == "straight"
  end

  def bend?
    @type == "bend"
  end

  def length
    if @type == "straight"
      @length
    else
      (@radius * Math::PI) * (@angle / 180.0)
    end
  end

  def length_on_lane(lane)
    if @type == "straight"
      @length
    else
      direction = @angle >= 0 ? -1 : 1
      new_radius = @radius + (direction * lane.distance_from_center)
      (new_radius * Math::PI) * (@angle.abs / 180.0)
    end
  end

  def length_on_switched_lane(from_lane, to_lane)
    if @type == "straight"
      delta = (to_lane.distance_from_center -  from_lane.distance_from_center)
      Math.sqrt(@length**2 + delta**2)
    else
      (length_on_lane(from_lane) + length_on_lane(to_lane)) / 2
    end
  end

  def disposition_sideways
    if @type == "straight"
      0
    else
      # For left turn, set factor to -1
      factor = @angle < 0 ? -1 : 1
      factor * (@radius * -1 * Math.cos(@angle) + @radius)
    end
  end

  def disposition_forward
    if @type == "straight"
      @length
    else
      @radius * Math.sin(@angle)
    end
  end
end


class Lane
  # A lane is part of a track.

  attr_reader :index, :distance_from_center

  def initialize(data)
    @index = data["index"]
    @distance_from_center = data["distanceFromCenter"]
  end
end


class Position
  # A position belongs to a car, and states a car's position.
  # (The game tick of this position will be stored along the car's position.)

  attr_reader :angle, :piece_index, :in_piece_distance, :start_lane, :end_lane, :lap
  
  def initialize(data)
    @angle = data["angle"]
    @piece_index = data["piecePosition"]["pieceIndex"]
    @in_piece_distance = data["piecePosition"]["inPieceDistance"]
    @start_lane = data["piecePosition"]["lane"]["startLaneIndex"]
    @end_lane = data["piecePosition"]["lane"]["endLaneIndex"]
    @lap = data["piecePosition"]["lap"]
  end
end
