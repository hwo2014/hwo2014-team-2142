require "simplecov"
require "test/unit"

SimpleCov.start

require_relative "hwo.rb"
require "json"

class TestGame < Test::Unit::TestCase
  def test_new
    car = Car.new({"name" => "Tron", "color" => "blue"})
    game = Game.new("game_id", car)

    # Testing game (and car) attributes
    assert_equal(car, game.my_car)
    assert_equal("blue", game.my_car.color)
    assert_equal("Tron", game.my_car.name)
    assert_equal("game_id", game.game_id)
  end

  def test_update_race
    game = Game.new("gamed_id", Car.new({"name" => "Tron", "color" => "black"}))

    race_data = {"track"=> {
              "id"=>"keimola",
              "name"=>"Keimola",
              "pieces"=> [{"length"=>100.0}, {"length"=>100.0, "switch"=>true}],
              "lanes"=> [{"distanceFromCenter"=>10, "index"=>1}],
              "startingPoint"=> {"position"=>{"x"=>-300.0, "y"=>-44.0}, "angle"=>90.0}},
            "cars"=> [{"id"=>{"name"=>"Tron", "color"=>"red"}, "dimensions"=>{"length"=>40.0, "width"=>20.0, "guideFlagPosition"=>10.0}}],
            "raceSession"=>{ "laps"=>3, "maxLapTimeMs"=>60000, "quickRace"=>true}}
    game.race = Race.new(race_data)

    # Testing updated race attributes
    assert_equal("keimola", game.race.track.id)
    assert_equal("Keimola", game.race.track.name)
    assert_equal(2, game.race.track.pieces.length)
    assert_equal(1, game.race.track.lanes.length)
    assert_equal(1, game.race.cars.length)
    assert_equal(3, game.race.session.laps)

  end

  def test_update_tick
    game = Game.new("game_id", Car.new({"name" => "Tron", "color" => "green"}))
    game.current_tick = 3

    assert_equal(3, game.current_tick)
  end
end

class TestRace < Test::Unit::TestCase
  def test_new
    data = {"track"=> {
              "id"=>"keimola",
              "name"=>"Keimola",
              "pieces"=> [
                {"length"=>100.0}, {"length"=>100.0}, {"length"=>100.0}, {"length"=>100.0, "switch"=>true},
                {"radius"=>100, "angle"=>45.0}, {"radius"=>100, "angle"=>45.0},
                {"radius"=>100, "angle"=>45.0}, {"radius"=>100, "angle"=>45.0},
                {"radius"=>200, "angle"=>22.5, "switch"=>true},
                {"length"=>100.0}, {"length"=>100.0},
                {"radius"=>200, "angle"=>-22.5},
                {"length"=>100.0}, {"length"=>100.0, "switch"=>true},
                {"radius"=>100, "angle"=>-45.0}, {"radius"=>100, "angle"=>-45.0},
                {"radius"=>100, "angle"=>-45.0}, {"radius"=>100, "angle"=>-45.0},
                {"length"=>100.0, "switch"=>true},
                {"radius"=>100, "angle"=>45.0}, {"radius"=>100, "angle"=>45.0},
                {"radius"=>100, "angle"=>45.0}, {"radius"=>100, "angle"=>45.0},
                {"radius"=>200, "angle"=>22.5}, {"radius"=>200, "angle"=>-22.5},
                {"length"=>100.0, "switch"=>true},
                {"radius"=>100, "angle"=>45.0}, {"radius"=>100, "angle"=>45.0},
                {"length"=>62.0},
                {"radius"=>100, "angle"=>-45.0, "switch"=>true}, {"radius"=>100, "angle"=>-45.0},
                {"radius"=>100, "angle"=>45.0}, {"radius"=>100, "angle"=>45.0},
                {"radius"=>100, "angle"=>45.0}, {"radius"=>100, "angle"=>45.0},
                {"length"=>100.0, "switch"=>true}, {"length"=>100.0}, {"length"=>100.0}, {"length"=>100.0},
                {"length"=>90.0}],
              "lanes"=> [
                {"distanceFromCenter"=>-10, "index"=>0},
                {"distanceFromCenter"=>10, "index"=>1}],
              "startingPoint"=> {
                "position"=>{"x"=>-300.0, "y"=>-44.0}, "angle"=>90.0}},
            "cars"=> [
              {"id"=>{"name"=>"Tron", "color"=>"red"}, "dimensions"=>{"length"=>40.0, "width"=>20.0, "guideFlagPosition"=>10.0}}],
            "raceSession"=>{
              "laps"=>3,
              "maxLapTimeMs"=>60000,
              "quickRace"=>true}}
    race = Race.new(data)
    
    # Testing track
    assert_equal("keimola", race.track.id)
    assert_equal("Keimola", race.track.name)
    assert_equal(40, race.track.pieces.length)
    assert_equal(2, race.track.lanes.length)

    # Testing cars
    assert_equal(1, race.cars.length)

    # Testing session
    assert_equal(3, race.session.laps)
    assert_equal(60000, race.session.max_lap_time_ms)
    assert_equal(true, race.session.quick_race)
  end
end


class TestSession < Test::Unit::TestCase
  def test_new
    session = Session.new({"laps"=>3, "maxLapTimeMs"=>60000, "quickRace"=>true})

    # Testing session attributes
    assert_equal(3, session.laps)
    assert_equal(60000, session.max_lap_time_ms)
    assert_equal(true, session.quick_race)
  end
end


class TestCar < Test::Unit::TestCase
  def test_new
    car = Car.new({"name" => "Tron", "color" => "red"})

    # Testing car attributes
    assert_equal("Tron", car.name)
    assert_equal("red", car.color)
  end

  def test_new_with_unknown_attributes
    car = Car.new({"no_name" => "Tron", "no_color" => "red"})

    # Testing attributes that were not set
    assert_not_equal("Tron", car.name)
    assert_not_equal("red", car.color)
  end

  def test_set_dimensions
    car = Car.new({"name" => "Tron", "color" => "red"})
    car.set_dimensions({"length" => 40.0, "width" => 20.0, "guideFlagPosition" => 10.0})

    # Testing updated car dimensions
    assert_equal(40.0, car.length)
    assert_equal(20.0, car.width)
    assert_equal(10.0, car.guide_flag_position)
  end

  def test_add_position
    car = Car.new({"name" => "Tron", "color" => "red"})
    position = Position.new({"id"=>{"name"=>"Tron", "color"=>"red"},
                             "angle"=>40.0,
                             "piecePosition"=> {
                               "pieceIndex"=>3,
                               "inPieceDistance"=>30.0,
                               "lane"=>{"startLaneIndex"=>0, "endLaneIndex"=>1},
                               "lap"=>4}})
    car.add_position(position, 1234)
  end

  def test_add_position_for_earlier_tick
    car = Car.new({"name" => "Tron", "color" => "red"})
    position = Position.new({"id"=>{"name"=>"Tron", "color"=>"red"},
                             "angle"=>40.0,
                             "piecePosition"=> {
                               "pieceIndex"=>3,
                               "inPieceDistance"=>30.0,
                               "lane"=>{"startLaneIndex"=>0, "endLaneIndex"=>1},
                               "lap"=>4}})
    car.add_position(position, 111)
  
    position = Position.new({"id"=>{"name"=>"Tron", "color"=>"red"},
                             "angle"=>35.0,
                             "piecePosition"=> {
                               "pieceIndex"=>3,
                               "inPieceDistance"=>12.0,
                               "lane"=>{"startLaneIndex"=>0, "endLaneIndex"=>0},
                               "lap"=>4}})
    car.add_position(position, 110)

    # Test that current car position is not updated, because game tick was earlier than last
    assert_not_equal(35.0, car.current_position.angle)
    assert_not_equal(12.0, car.current_position.in_piece_distance)
    assert_not_equal(0, car.current_position.end_lane)
  end

  # positions

  # current_position
  # current_distance
  # current_velocity
  # current_acceleration
end


class TestTrack < Test::Unit::TestCase
  def test_new
    data = {"id"=>"keimola",
            "name"=>"Keimola",
            "pieces"=> [
              {"length"=>100.0},
              {"length"=>100.0, "switch"=>true},
              {"radius"=>100, "angle"=>45.0},
              {"radius"=>200, "angle"=>22.5, "switch"=>true}],
            "lanes"=> [
              {"distanceFromCenter"=>-10, "index"=>0},
              {"distanceFromCenter"=>10, "index"=>1}],
            "startingPoint"=> {
              "position"=>{"x"=>-300.0, "y"=>-44.0}, "angle"=>90.0}
           }

    track = Track.new(data)
    
    # Testing track attributes
    assert_equal("keimola", track.id)
    assert_equal("Keimola", track.name)
    assert_equal(4, track.pieces.length)
    assert_equal(2, track.lanes.length)
  end

  def test_upcoming_pieces
    data = {"id"=>"keimola",
            "name"=>"Keimola",
            "pieces"=> [
              {"length"=>100.0}, # 0
              {"length"=>100.0, "switch"=>true}, # 1
              {"length"=>100.0}, # 2
              {"radius"=>100, "angle"=>45.0}] # 3
           }
    track = Track.new(data)

    assert_equal([0,1,2,3], track.upcoming_pieces(0,4))
    assert_equal([0,1], track.upcoming_pieces(0,2))
    assert_equal([2,3,0,1], track.upcoming_pieces(2,4))
    assert_equal([2,3], track.upcoming_pieces(2,2))
    assert_equal([3,0,1,2], track.upcoming_pieces(3,4))
    assert_equal([3,0], track.upcoming_pieces(3,2))
  end

  def test_switch_before_turn
    data = {"id"=>"keimola",
            "name"=>"Keimola",
            "pieces"=> [
              {"length"=>100.0}, # 0
              {"length"=>100.0, "switch"=>true}, # 1
              {"radius"=>100, "angle"=>45.0}, # 2
              {"length"=>100.0}, # 3
              {"radius"=>200, "angle"=>22.5, "switch"=>true}, # 4
              {"length"=>100.0}, # 5
              {"length"=>100.0}, # 6
              {"radius"=>100, "angle"=>45.0}] # 7
           }
    track = Track.new(data)

    assert_equal(true, track.switch_before_turn?(0))
    assert_equal(false, track.switch_before_turn?(1)) # Can't use switch on current piece
    assert_equal(false, track.switch_before_turn?(2))
    assert_equal(true, track.switch_before_turn?(3)) # Upcoming switch in turn
    assert_equal(false, track.switch_before_turn?(4))
    assert_equal(false, track.switch_before_turn?(5))
    assert_equal(false, track.switch_before_turn?(6))
    assert_equal(false, track.switch_before_turn?(7))
  end
  
  def test_length_of_straight_pieces_ahead
    data = {"id"=>"keimola",
            "name"=>"Keimola",
            "pieces"=> [
              {"length"=>100.0}, # 0
              {"length"=>100.0, "switch"=>true}, # 1
              {"radius"=>100, "angle"=>45.0}, # 2
              {"length"=>100.0}, # 3
              {"radius"=>200, "angle"=>22.5, "switch"=>true}, # 4
              {"length"=>100.0}, # 5
              {"length"=>100.0}, # 6
              {"length"=>100.0}] # 7
           }
    track = Track.new(data)

    assert_equal(2, track.length_of_straight_pieces_ahead(0))
    assert_equal(1, track.length_of_straight_pieces_ahead(1))
    assert_equal(0, track.length_of_straight_pieces_ahead(2))
    assert_equal(1, track.length_of_straight_pieces_ahead(3))
    assert_equal(0, track.length_of_straight_pieces_ahead(4))
    assert_equal(5, track.length_of_straight_pieces_ahead(5))
    assert_equal(4, track.length_of_straight_pieces_ahead(6))
    assert_equal(3, track.length_of_straight_pieces_ahead(7))
  end

  def test_no_switch_before_turn
    data = {"id"=>"keimola",
            "name"=>"Keimola",
            "pieces"=> [
              {"length"=>100.0}, # 0
              {"radius"=>100, "angle"=>180.0}, # 1
              {"length"=>100.0}, # 2
              {"radius"=>100, "angle"=>180.0}] # 3
           }
    track = Track.new(data)

    assert_equal(false, track.switch_before_turn?(0))
    assert_equal(false, track.switch_before_turn?(1))
    assert_equal(false, track.switch_before_turn?(2))
    assert_equal(false, track.switch_before_turn?(3))
  end

  def test_distance_in_lap
    # TODO distance = track.distance_in_lap(position)
  end

  def test_distance_in_race
    # TODO distance = track.distance_in_race(position)
  end
end



class TestPiece < Test::Unit::TestCase
  def test_new
    piece1 = Piece.new({"length"=>100.0})
    piece2 = Piece.new({"length"=>100.0, "switch"=>true})
    piece3 = Piece.new({"radius"=>100, "angle"=>45.0})
    piece4 = Piece.new({"radius"=>200, "angle"=>22.5, "switch"=>true})

    # Testing several types of track pieces
    assert_equal(100.0, piece1.length)
    assert_equal(true, piece1.straight?)
    assert_equal(false, piece1.bend?)
    assert_equal(false, piece1.switch?)

    assert_equal(true, piece2.switch?)

    assert_equal(true, piece3.bend?)
    assert_equal(false, piece3.straight?)
    assert_equal(100, piece3.radius)
    assert_equal(45.0, piece3.angle)
    assert_equal(false, piece3.switch?)

    assert_equal(true, piece4.switch?)
  end

  def test_length
    piece1 = Piece.new({"length"=>100.0})
    piece2 = Piece.new({"length"=>200.0, "switch"=>true})
    piece3 = Piece.new({"radius"=>100, "angle"=>45.0})
    piece4 = Piece.new({"radius"=>200, "angle"=>22.5, "switch"=>true})

    assert_equal(100.0, piece1.length)
    assert_equal(200.0, piece2.length)

    assert_equal((100.0 * Math::PI) * (45.0 / 180), piece3.length)
    assert_equal((200.0 * Math::PI) * (22.5 / 180), piece4.length)
  end

  def test_length_on_straight_lane
    piece1 = Piece.new({"length"=>100.0})
    lane1 = Lane.new({"distanceFromCenter"=>-10, "index"=>0})
    lane2 = Lane.new({"distanceFromCenter"=>  0, "index"=>1})
    lane3 = Lane.new({"distanceFromCenter"=> 10, "index"=>2})

    assert_equal(100, piece1.length_on_lane(lane1))
    assert_equal(100, piece1.length_on_lane(lane2))
    assert_equal(100, piece1.length_on_lane(lane3))
  end

  def test_length_on_bent_lane
    piece2 = Piece.new({"radius"=>100, "angle"=> 45.0})
    piece3 = Piece.new({"radius"=>100, "angle"=>-90.0})
    lane1 = Lane.new({"distanceFromCenter"=>-10, "index"=>0})
    lane2 = Lane.new({"distanceFromCenter"=>  0, "index"=>1})
    lane3 = Lane.new({"distanceFromCenter"=> 10, "index"=>2})

    # Test right turns
    assert_equal((110.0 * Math::PI) * 0.25, piece2.length_on_lane(lane1))
    assert_equal((100.0 * Math::PI) * 0.25, piece2.length_on_lane(lane2))
    assert_equal((90.0 * Math::PI) * 0.25, piece2.length_on_lane(lane3))

    # Test left turns
    assert_equal((90.0 * Math::PI) * 0.5, piece3.length_on_lane(lane1))
    assert_equal((100.0 * Math::PI) * 0.5, piece3.length_on_lane(lane2))
    assert_equal((110.0 * Math::PI) * 0.5, piece3.length_on_lane(lane3))
  end

  def test_length_with_switched_lane
    piece1 = Piece.new({"length"=>200.0, "switch"=>true})
    piece2 = Piece.new({"radius"=>200, "angle"=>90.0, "switch"=>true})
    lane1 = Lane.new({"distanceFromCenter"=>-10, "index"=>0})
    lane2 = Lane.new({"distanceFromCenter"=>  0, "index"=>1})
    lane3 = Lane.new({"distanceFromCenter"=> 20, "index"=>2})

    # Test switch on straight pieces
    assert_equal((Math.sqrt(200.0**2 + 10.0**2)), piece1.length_on_switched_lane(lane1, lane2))
    assert_equal((Math.sqrt(200.0**2 + 20.0**2)), piece1.length_on_switched_lane(lane2, lane3))

    # Test switch on bent pieces
    assert_equal((190.0 * Math::PI) * 0.5, piece2.length_on_switched_lane(lane2, lane3))
  end

  def test_disposition_straight
    # FIXME Not correct if piece is positioned at an angle.
    piece = Piece.new({"length"=>200.0})

    assert_equal(0.0, piece.disposition_sideways)
    assert_equal(200.0, piece.disposition_forward)
  end

  def test_disposition_bent
    # FIXME Not correct if piece starts at another angle.
    piece1 = Piece.new({"radius"=>100, "angle"=> 30.0})
    piece2 = Piece.new({"radius"=>200, "angle"=>-60.0})

    assert_equal(-100*Math.cos(30)+100, piece1.disposition_sideways)
    assert_equal(100*Math.sin(30), piece1.disposition_forward)

    assert_equal(200*Math.cos(-60)-200, piece2.disposition_sideways)
    assert_equal(200*Math.sin(-60), piece2.disposition_forward)
  end
end


class TestLane < Test::Unit::TestCase
  def test_new
    lane = Lane.new({"distanceFromCenter"=>-10, "index"=>0})
    
    # Testing lane attributes
    assert_equal(-10, lane.distance_from_center)
    assert_equal(0, lane.index)
  end
end


class TestPosition < Test::Unit::TestCase
  def test_new
    position = Position.new({"id"=>{"name"=>"Tron", "color"=>"red"},
                             "angle"=>40.0,
                             "piecePosition"=> {
                               "pieceIndex"=>3,
                               "inPieceDistance"=>30.0,
                               "lane"=>{"startLaneIndex"=>0, "endLaneIndex"=>1},
                               "lap"=>4}})

    # Testing position attributes
    assert_equal(40.0, position.angle)
    assert_equal(3, position.piece_index)
    assert_equal(30.0, position.in_piece_distance)
    assert_equal(0, position.start_lane)
    assert_equal(1, position.end_lane)
    assert_equal(4, position.lap)
  end
end
